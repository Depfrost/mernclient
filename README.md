## About The Project

This project allow users to post 'memories' consisting of a title, a message, an image and tags.

Users will be able to:
	- Sign up, sign in, sign out, with or without google authentication.
	- Create, delete and edit a memory
	- Search for a memory by title or by tags
	- See a memory details and related memories
	- Like and Comment a memory

This repository contains the Front End part of the project.

### Built With

* [Vite](https://vitejs.dev/)
* [React.js](https://reactjs.org/)
* [Redux](https://redux.js.org/)
* [Material UI](https://mui.com/)

## Getting Started

### Prerequisites

* Run the back end of the application (server + database). Code available [here.](https://gitlab.com/Depfrost/mernserver)
* Get your Google ID if you want to be able to login via Google services.

### Installation

1. Clone the repo
   ```sh
   git clone https://gitlab.com/Depfrost/mernclient.git
   ```
2. Rename the file `.env.exemple` to `.env` and provide the server URL and your Google ID.

4. Install Yarn packages
   ```sh
   yarn install
   ```
5. Run the project
   ```sh
	yarn run dev
	```


## License

Distributed under the MIT License.

## Acknowledgments

This project has been made with the help of the JavaScriptMastery's tutorial on MERN stack.